(function () {
    angular
        .module("uirouterApp")
        .controller("PeopleCtrl", ["PeopleService",  PeopleCtrl]);

    function PeopleCtrl( PeopleService ){
        var vm = this;
        console.log("PeopleCtrl controller --> ");
        PeopleService.getAllPeople().then(function(result){
            vm.people = result;
        });
        console.log("PeopleCtrl controller --> ", vm.people);
    }

})();